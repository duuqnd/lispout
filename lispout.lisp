;;;; lispout.lisp
;;
;;;; Copyright (c) 2019 John Lorentzson (Duuqnd)


(in-package #:lispout)

(defparameter *debug-color* (gamekit:vec4 1 1 1 1.5))
(defparameter *standalone-executable* nil)
(defparameter *font* nil)

(defun reset-game ()
  (defparameter *current-color* 3)
  (change-color *current-color*)
  
  (defvar *paddle-position* (gamekit:vec2 400 32))
  (defparameter *paddle-speed* 8)

  (defparameter *ball-size* 15)
  (defparameter *ball-position* (gamekit:vec2 400 300))
  (defparameter *ball-x-velocity* 0)
  (defparameter *ball-y-velocity* -1)
  (defparameter *ball-speed* 5)

  (defparameter *single-stepping* nil)

  (defparameter *input* (list 'left nil 'right nil))

  (defparameter *brick-row-length* 4)
  (defparameter *bricks* (list (make-list 4 :initial-element t) (make-list 4 :initial-element t)
                               (make-list 4 :initial-element t) (make-list 4 :initial-element t))))

(gamekit:defgame lispout () ())

(defun start-game ()
  ;;(gamekit:define-font 'game-font "font.ttf")
  ;;(defparameter *font* (gamekit:make-font 'game-font 32))
  (format t "Lispout was created by Duuqnd (John Lorentzson) and is licensed under the MIT License~%")
  (format t "Lispout uses trivial-gamekit and cl-bodge created by borodust (Pavel Korolev). See CREDITS file for licenses.~%")
  (gamekit:start 'lispout))

(defun export-game ()
  (setf *standalone-executable* t)
  (sb-ext:gc :full t)
  (sb-ext:save-lisp-and-die "lispout" :compression t :executable t))

(defmethod gamekit:post-initialize ((game lispout))
  (reset-game)
  (gamekit:bind-button :left :pressed (lambda () (setf (getf *input* 'left) t)))
  (gamekit:bind-button :left :released (lambda () (setf (getf *input* 'left) nil)))

  (gamekit:bind-button :right :pressed (lambda () (setf (getf *input* 'right) t)))
  (gamekit:bind-button :right :released (lambda () (setf (getf *input* 'right) nil)))

  (gamekit:bind-button :f1 :pressed #'do-update)
  (gamekit:bind-button :escape :pressed #'reset-game))

(defmethod gamekit:pre-destroy ((game lispout))
  (if *standalone-executable* (sb-ext:quit)))

(defmethod gamekit:draw ((game lispout))
  (gamekit:draw-rect (gamekit:vec2 0 0) 800 600 :fill-paint *current-color*)
  (draw-paddle)
  (draw-ball)
  (draw-bricks *bricks*)
  ;;(debug-display)
  ;;(gamekit:draw-text "λ" (gamekit:vec2 (- (gamekit:x *ball-position*) 14) (- (gamekit:y *ball-position*) 10)) :font *font*)
  )

(defmethod gamekit:act ((game lispout))
  (if (not *single-stepping*) (do-update)))

(defun toggle-single-step ()
  (setf *single-stepping* (not *single-stepping*)))

(defun do-update ()
  (update-paddle)
  (update-ball)
  (handle-collision))

(defun debug-display ()
  (gamekit:draw-text (get-input-as-string 0) (gamekit:vec2 0 588) :fill-color *debug-color*)
  (gamekit:draw-text (get-input-as-string 1) (gamekit:vec2 0 568) :fill-color *debug-color*)
  (gamekit:draw-text (get-input-as-string 2) (gamekit:vec2 0 548) :fill-color *debug-color*)
  (gamekit:draw-text (get-input-as-string 3) (gamekit:vec2 0 528) :fill-color *debug-color*)
  (gamekit:draw-text (get-input-as-string 4) (gamekit:vec2 0 508) :fill-color *debug-color*))

(defun get-input-as-string (input-number)
  (with-output-to-string (stream)
    (case input-number
      (0 (format stream "Left: ~a" (left-pressed?)))
      (1 (format stream "Right: ~a" (right-pressed?)))
      (2 (format stream "Ball intersecting paddle: ~a"
                 (ball-intersecting-paddle? (gamekit:x *ball-position*) (gamekit:y *ball-position*)
                                            (gamekit:x *paddle-position*) (gamekit:y *paddle-position*))))
      (3 (format stream "Ball Y velocity: ~a    Ball X velocity: ~a" *ball-y-velocity* *ball-x-velocity*))
      (4 (format stream "Intersections: ~a" (mapcar #'ball-intersecting-block? '(0 0 0 0 1 1 1 1 2 2 2 2) '(0 1 2 3 0 1 2 3 0 1 2 3)))))))

(defun left-pressed? ()
  (getf *input* 'left))

(defun right-pressed? ()
  (getf *input* 'right))

(defun bounce-angle (angle )
  (if (< angle pi)
      (- 0 angle)
      (- pi angle)))

(defun update-paddle ()
  (if (left-pressed?) (decf (gamekit:x *paddle-position*) *paddle-speed*))
  (if (right-pressed?) (incf (gamekit:x *paddle-position*) *paddle-speed*)))

(defun update-ball ()
  (let ((x (gamekit:x *ball-position*))
        (y (gamekit:y *ball-position*)))
    (setf x (+ x (* *ball-speed* *ball-x-velocity*)))
    (setf y (+ y (* *ball-speed* *ball-y-velocity*)))
    (if (< y 0) (progn (setf y 300 x 400 *ball-x-velocity* 0) (change-color 3) (reset-game)))
    (setf (gamekit:x *ball-position*) x)
    (setf (gamekit:y *ball-position*) y)))

(defun change-color (side)
  (setf *current-color*
        (case side
          (0 (gamekit:vec4 0.3 0.4 0.7 1))
          (1 (gamekit:vec4 0.1 0.6 0.6 1))
          (2 (gamekit:vec4 0.1 0.6 0.4 1))
          (3 (gamekit:vec4 1 0.2 0.2 1)))))

(defun ball-intersecting-paddle? (ball-x ball-y paddle-x paddle-y)
  (let ((ball-right-edge (+ ball-x (/ *ball-size* 2)))
        (ball-left-edge (- ball-x (/ *ball-size* 2)))
        (ball-top-edge (+ ball-y (/ *ball-size* 2)))
        (ball-bottom-edge (- ball-y (/ *ball-size* 2)))
        (paddle-right-edge (+ paddle-x 64))
        (paddle-left-edge paddle-x)
        (paddle-top-edge (+ paddle-y 32))
        (paddle-bottom-edge paddle-y))
    (if (and (> ball-right-edge paddle-left-edge)
             (< ball-left-edge paddle-right-edge)
             (> ball-top-edge paddle-bottom-edge)
             (< ball-bottom-edge paddle-top-edge))
        t
        nil)))

(defun ball-intersecting-block? (block-row block-column)
  ;;(if (not (nth block-column (nth block-row *bricks*))) (return-from ball-intersecting-block? nil))
  (let* ((ball-x (gamekit:x *ball-position*))
         (ball-y (gamekit:y *ball-position*))
         (ball-right-edge (+ ball-x (/ *ball-size* 2)))
         (ball-left-edge (- ball-x (/ *ball-size* 2)))
         (ball-top-edge (+ ball-y (/ *ball-size* 2)))
         (ball-bottom-edge (- ball-y (/ *ball-size* 2)))

         (block-x (* 140 (1+ block-column)))
         (block-y (- 500 (* 50 block-row)))
         
         (block-right-edge (+ block-x 80))
         (block-left-edge block-x)
         (block-top-edge (+ block-y 40))
         (block-bottom-edge block-y))
    (if (and (> ball-right-edge block-left-edge)
             (< ball-left-edge block-right-edge)
             (> ball-top-edge block-bottom-edge)
             (< ball-bottom-edge block-top-edge))
        t
        nil)))

(defun handle-block-row-collision (row)
  (loop for column from 0 to 3 do
    (if (ball-intersecting-block? row column) (setf (nth column (nth row *bricks*)) nil))))

(defun handle-block-collision ()
  (loop for i from 0 to 3 do
    (handle-block-row-collision i)))

(defun handle-collision ()
  (let ((ball-x (gamekit:x *ball-position*))
        (ball-y (gamekit:y *ball-position*)))
    (when (> (+ ball-y *ball-size*) 600)
      (change-color 0)
      (setf ball-y (- 600 *ball-size*))
      (setf *ball-y-velocity* (- *ball-y-velocity*)))
    (when (> (+ ball-x *ball-size*) 800)
      (change-color 1)
      (setf ball-x (- 800 *ball-size*))
      (setf *ball-x-velocity* (- *ball-x-velocity*)))
    (when (< (- ball-x *ball-size*) 0)
      (change-color 2)
      (setf ball-x *ball-size*)
      (setf *ball-x-velocity* (- *ball-x-velocity*)))

    (when (ball-intersecting-paddle? (gamekit:x *ball-position*) (gamekit:y *ball-position*)
                                     (gamekit:x *paddle-position*) (gamekit:y *paddle-position*))
      (setf *ball-y-velocity* (- *ball-y-velocity*))
      (if (>= (gamekit:x *ball-position*) (+ (gamekit:x *paddle-position*) 32))
          (setf *ball-x-velocity* 1)
          (setf *ball-x-velocity* -1)))
    (handle-block-collision)))

(defun draw-brick-row (brick-row y)
  (loop for i from 0 to (1- *brick-row-length*) do
    (if (nth i (nth brick-row *bricks*))
        (gamekit:draw-rect (gamekit:vec2 (* 140 (1+ i)) y) 80 40
                           :fill-paint (gamekit:vec4 (* 0.01 (random 40)) (* 0.01 (random 40)) (* 0.01 (random 40)) 1)))))

(defun draw-bricks (bricks)
  (loop for i from 0 to 3 do
    (draw-brick-row i (- 500 (* 50 i)))))

(defun draw-paddle ()
  (gamekit:draw-rect *paddle-position* 64 32 :fill-paint (gamekit:vec4 0.1 0.1 1 1)))

(defun draw-ball ()
  (gamekit:draw-circle *ball-position* *ball-size* :fill-paint (gamekit:vec4 1 0 0 1)))
