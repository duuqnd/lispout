# Lispout is a BAD breakout clone, AND I KNOW THAT IT'S BAD!! You don't have to tell me.

Lispout is a bad, incomplete and unfun breakout clone written for the 2019 Lisp Game Jam.

It's bad and incomplete because I suck at writing games and I'm still quite new to Lisp.

My goal with this game was not to make a bad breakout clone. It was to *finish* a bad breakout clone,
and I think I achived that goal.

## Running the game

If you want to run the binary version, just run ``` launch.sh ```.

If you want to run the game from the source code, do the following:

First, run ``` (ql-dist:install-dist "http://bodge.borodust.org/dist/org.borodust.bodge.txt") ``` in Lisp if you haven't done so before.
After that, simply load the package using ``` (ql:quickload :lispout) ``` and do ``` (lispout::start-game) ```.

If the game is compiled to an executable, it will exit when the window is closed.
If you're running the game from the source code, you can exit Lisp from the REPL.

## License

MIT License


Copyright (c) 2019 John Lorentzson (Duuqnd)


