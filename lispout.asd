;;;; lispout.asd
;;
;;;; Copyright (c) 2019 John Lorentzson (Duuqnd)


(asdf:defsystem #:lispout
  :description "Describe lispout here"
  :author "John Lorentzson (Duuqnd)"
  :license  "MIT License"
  :version "0.0.1"
  :serial t
  :depends-on (#:trivial-gamekit)
  :components ((:file "package")
               (:file "lispout")))
